# tp1_ex5

on retrouve ici deux packages : test et calculator

pour executer ce code on utilise la commande : python test/test.py


amélioration apporté au code calculator.py de l'exercice 4 :
-ajout de la detection de division par 0 qui retourne un message d'erreur
-ajout de la detection de nombres complexes non-entier (partie imaginaire et réelle appartenant à l'ensemble des décimaux) qui retourne un message d'erreur.

bien évidemment j'ai ajouté des nouvelles opérations dans le code test.py afin de vérifier le bon fonctionnement des améliorations du code calculator.py 
