# Tester la classe
from calculator.calculator import SimpleComplexCalculator
complex1 = [4, 5]  # Premier nombre complexe
complex2 = [2, 3]  # Deuxième nombre complexe

calculator = SimpleComplexCalculator()

print(
    "Somme :", calculator.complex_sum(complex1, complex2)
)  # Affiche la somme des deux nombres complexes
print(
    "Différence :", calculator.complex_subtract(complex1, complex2)
)  # Affiche la différence des deux nombres complexes
print(
    "Produit :", calculator.complex_multiply(complex1, complex2)
)  # Affiche le produit des deux nombres complexes
print(
    "Division :", calculator.complex_divide(complex1, complex2)
)  # Affiche la division des deux nombres complexes

try:
    print(
        "Division :", calculator.complex_divide(complex1, [0, 0])
    )  # Affiche la division des deux nombres complexes
except ZeroDivisionError:
    print("Division par zéro impossible")

try:
    print(
        "somme :", calculator.complex_sum(complex1, [4.5, 5])
    )
except TypeError:
    print("Erreur de type")


